//Modo Estricto
'use strict'

//Prueba con Let y Var

//Prueba con Var
var numero = 40;
console.log(numero); //Valor 40

if (true) {
    var numero = 50
    console.log(numero); //valor 50
}

console.log(numero); //valor 50

//Prueba con Let
var texto = "Curso de Javascript";
console.log(texto);

if(true){
    let texto = "Curso de HTML";
    console.log(texto);
}
console.log(texto);