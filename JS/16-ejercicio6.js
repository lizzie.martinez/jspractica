//Modo Estricto
'use strict'

//Muestre si un número es par o impar con las siguientes indicaciones:
    //a) Prompt.
    //b) Si no es válido que nos pida de nuevo el número.


var numero = parseInt(prompt("Introduce un número", 1));

while(isNaN(numero)){
    var numero = parseInt(prompt("Introduce un número", 1));
}
if(numero % 2 == 0){
    alert("El número es par");
}else{
    alert("El número es impar");
}
