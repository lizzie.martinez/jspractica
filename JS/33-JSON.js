//Modo Estricto
'use strict'

//JSON - Javascript Object Notation

var pelicula = {
    titulo: 'Batman vs Superman',
    year: 2017,
    pais: 'Estados Unidos'
};

var peliculas = [
    {titulo: "Cars", year: 2006, pais: 'Estados Unidos' },
    pelicula
];

var cajaPeliculas = document.querySelector("#peliculas");
var index;
for (index in peliculas){
    var p = document.createElement("p");
    p.append(peliculas[index].titulo + "    -   " + peliculas[index].year);
    cajaPeliculas.append(p);
}

console.log(pelicula);