//Modo Estricto
'use strict'

//Operadores Lógicos
var numero1 = 7; 
var numero2 = 12;
var operacion = numero1 * numero2;

console.log("el resultado de la operación es: "+operacion);

//Tipos de Datos
var numeroEntero = 44;
var cadenaTexto = "Hola";
console.log(cadenaTexto);
var verdadFalso = true;
console.log(verdadFalso);


var numeroFalso = "33";
console.log(numeroEntero + ' Texto Concatenado');

//Se utiliza typeOf para reconocer qué tipo de dato es el que se utiliza.
console.log(typeof numeroEntero);
console.log(typeof cadenaTexto);
console.log(typeof verdadFalso);
console.log(typeof numeroFalso);