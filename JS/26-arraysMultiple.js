'use-strict'

//Arrays dentro de otros arrays

var categorias = ["Acción", "Terror", "Comedia"];
var peliculas = ["Star Wars", "Psicosis", "Shrek"];

var cine = [categorias, peliculas];

//console.log(cine[0][1]);
//onsole.log(cine[1][2]);


//Operaciones con Arrays

var elemento = "";

do {
    elemento = prompt("Introduce tu película");
    peliculas.push(elemento);
} while (elemento != "ACABAR") 

peliculas[3] = undefined;

console.log(peliculas);